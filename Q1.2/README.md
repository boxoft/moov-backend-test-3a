# Q1.2

https://bitbucket.org/maypolepublic/backend-test-3a/src/master/Q1.2/



Please port the following C++ code to JavaScript

```c++
// language: C++
// environment: production

// generate temporary user id
// the temporary user id must be 18 decimal digits long, and starts with "10"
// so we are left with 16 digits, or 53 bits
int64_t cur_time = get_unix_time_in_milliseconds() & 0x3ffffffffff; // this is 42 bits, we're left with 11 bits
int64_t random   = arc4random_uniform(0x7ff) & 0x7ff;

user_id = 100000000000000000 | (cur_time << 11) | random;
```



## Development Environment

Windows 10

Node.js 14.16 LTS

Visual Studio Code 1.54



## Solutions

`Q1.2/README.md`



### Solution 1

`Q1.2/a1/solution.js`

```js
/**
 * @file The solution to question Q1.2.
 * @author He Yu <box@boxoft.net>
 * 
 * @description
 * - Convert `int64_t` to `BigInt`.
 * - Convert `get_unix_time_in_milliseconds()` to `Date.now()`.
 * - Convert `arc4random_uniform()` to `Math.random()`.
 * 
 * ## Q1.2
 * Please port the following C++ code to JavaScript
 *
 * ```c++
 * // language: C++
 * // environment: production
 *
 * // generate temporary user id
 * // the temporary user id must be 18 decimal digits long, and starts with "10"
 * // so we are left with 16 digits, or 53 bits
 * 
 * int64_t cur_time = get_unix_time_in_milliseconds() & 0x3ffffffffff; // this is 42 bits, we're left with 11 bits
 * int64_t random   = arc4random_uniform(0x7ff) & 0x7ff;
 *
 * user_id = 100000000000000000 | (cur_time << 11) | random;
 * ```
 */

let curTime = BigInt(Date.now()) & BigInt(0x3ffffffffff);
let random = BigInt(Math.floor((Math.random() * 0x7ff)) & 0x7ff);
let userId = 100000000000000000n | curTime << BigInt(11) | random;

console.log(curTime, random, userId);
```



* Convert `int64_t` to `BigInt`.

* Convert `get_unix_time_in_milliseconds()` to `Date.now()`.

* Convert `arc4random_uniform()` to `Math.random()`.



According to [arc4random()](https://man.openbsd.org/arc4random_uniform.3), 

> [`arc4random_uniform`](https://man.openbsd.org/arc4random_uniform.3#arc4random_uniform)() will return a single 32-bit value, uniformly distributed but less than upper_bound. This is recommended over constructions like “`arc4random() % upper_bound`” as it avoids "modulo bias" when the upper bound is not a power of two. In the worst case, this function may consume multiple iterations to ensure uniformity; see the source code to understand the problem and solution.

According to [Math.random()](https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Global_Objects/Math/random),

> The **`Math.random()`** function returns a floating-point, pseudo-random number in the range 0 to less than 1 (inclusive of 0, but not 1) with approximately uniform distribution over that range — which you can then scale to your desired range.

So I think it is not a bad solution to replace `arc4random_uniform()` with `Math.random()`.



The Node packages `secure-random-uniform` or `arc4random` might be better than `Math.random()`.



If, for some reason, the `BigInt` type is not allowed to use, then the alternative solutions could be using third-party packages or dividing 64 bits bitwise operations into two 32 bits bitwise operations.



## References

- BigInt - JavaScript | MDN
  https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Global_Objects/BigInt

- The Essential Guide To JavaScript’s Newest Data Type: BigInt
  https://www.smashingmagazine.com/2019/07/essential-guide-javascript-newest-data-type-bigint/

- arc4random(3) - OpenBSD manual pages
  https://man.openbsd.org/arc4random_uniform.3

- Math.random() - JavaScript | MDN
  https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Global_Objects/Math/random

- secure-random-uniform - npm
  https://www.npmjs.com/package/secure-random-uniform

- arc4random - npm
  https://www.npmjs.com/package/arc4random

- BigInteger.js
  https://github.com/peterolson/BigInteger.js

- long.js
  https://github.com/dcodeIO/long.js

- JSBI — pure-JavaScript BigInts
  https://github.com/GoogleChromeLabs/jsbi

- bitwise AND in Javascript with a 64 bit integer
  https://stackoverflow.com/questions/2983206/bitwise-and-in-javascript-with-a-64-bit-integer

- Emscripten
https://github.com/emscripten-core/emscripten

