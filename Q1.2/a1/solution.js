/**
 * @file The solution to question Q1.2.
 * @author He Yu <box@boxoft.net>
 * 
 * @description
 * - Convert `int64_t` to `BigInt`.
 * - Convert `get_unix_time_in_milliseconds()` to `Date.now()`.
 * - Convert `arc4random_uniform()` to `Math.random()`.
 * 
 * ## Q1.2
 * Please port the following C++ code to JavaScript
 *
 * ```c++
 * // language: C++
 * // environment: production
 *
 * // generate temporary user id
 * // the temporary user id must be 18 decimal digits long, and starts with "10"
 * // so we are left with 16 digits, or 53 bits
 * 
 * int64_t cur_time = get_unix_time_in_milliseconds() & 0x3ffffffffff; // this is 42 bits, we're left with 11 bits
 * int64_t random   = arc4random_uniform(0x7ff) & 0x7ff;
 *
 * user_id = 100000000000000000 | (cur_time << 11) | random;
 * ```
 */

let curTime = BigInt(Date.now()) & BigInt(0x3ffffffffff);
let random = BigInt(Math.floor((Math.random() * 0x7ff)) & 0x7ff);
let userId = 100000000000000000n | curTime << BigInt(11) | random;

console.log(curTime, random, userId);
