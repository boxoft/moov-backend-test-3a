# Q1.5

https://bitbucket.org/maypolepublic/backend-test-3a/src/master/Q1.5/



Tell us 15 of your working habits / styles... e.g. your terminal setup, email filters, productivity tools, communication patterns etc.



## My Answer

1. Keeping focus with "[Marinara: Pomodoro® Assistant](https://chrome.google.com/webstore/detail/marinara-pomodoro®-assist/lojgmehidjdhhbmpjfamhpkpodfcodef)".
2. Dividing a bar of instant coffee into two drinks, one in the morning and one in the afternoon.
3. Submitting daily reports to my supervisors when I working for companies or to my clients when I working for them.
4. Sending emails with Gmail or [Foxmail](https://www.foxmail.com/win/en/).
5. Using more than one large monitor if possible.
6. Writing code with Visual Studio Code, Notepad++, PyCharm, Atom, etc.
7. Comparing files or folders with [WinMerge](https://winmerge.org/).
8. Using [Postman](https://www.postman.com/) to build or test APIs.
9. Installing multiple Windows applications, such as Cmder, Everything, Git, Node.js, Python, etc. with [Chocolatey](https://chocolatey.org/).
10. Learning new things with YouTube.com, bilibili.com, etc.
11. Looking up English words with [Youdao Dictionary](http://dict.youdao.com/) and saving them with [Raindrop](https://raindrop.io/).
12. Translating Chinese or English text with [DeepL Translate](https://www.deepl.com/translator) and [Google Translate](https://translate.google.com/).
13. Checking the syntax of English text with [Grammarly](https://www.grammarly.com/).
14. Taking notes with [Obsidian](https://obsidian.md/), [Dynalist](https://dynalist.io/), etc.
15. Syncing files among several computers with Dropbox and [MEGA](https://mega.io/).
16. Organizing multiple windows with [WindowGrid](http://windowgrid.net/), [WinSplit Revolution](https://maxto.net/compare/winsplit_revolution/), or [WinNumpad Position](https://pbrs.weebly.com/win-numpad-positioner.html).
17. Quickly locating files with [Everything](https://www.voidtools.com/).
18. Creating and using Virtual machines with [VirtualBox](https://www.virtualbox.org/).
19. Creating and reading Markdown files with [Typora](https://typora.io/).
20. Running commands with Windows Command Prompt, [Cmder](https://cmder.net/), Windows Subsystem for Linux (WSL), or [Xshell](https://www.netsarang.com/en/xshell/).