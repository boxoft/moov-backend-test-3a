# Q1.4

https://bitbucket.org/maypolepublic/backend-test-3a/src/master/Q1.4/



Please plan the product listing & product editing pages for the web engineers to implement.

![spec](https://storage.googleapis.com/public-assets-maypole/interview-assets/image%201.png)

Please scaffold the vue.js project in "hello-maypole" folder. As the lead of the project, please demonstrate:

- how the data is stored
- how the data is uploaded / downloaded
- how the page transition is handled
- how the key NPM packages are used
- etc.

Please write a document (in this folder) for both the web engineers & server side engineers to

- maximize their productivity
- clearly define the exit criteria



## 1. Plan

The Web frontend developers should fully understand:

1. Pages and routes.
2. Detailed specifications of all the elements, such as labels, input fields, buttons, on the pages.

And they should report on uncertainties or other issues.



### 1.1 Pages and Routes

There are 2 kinds of pages since the **New Item** page and the **Edit Item** page are very alike. These 2 pages are the **My Store** page and the **New/Edit Item** page.



There are 12 pages shown in the above design. I added a number to each page.

![numbered](assets/design-w-numbers.png)



The pages that are actually the **My Store** page are #1, #5, #7, #8, and #9.

The pages that are actually the **New/Edit Item** page are #2, #3, #4, #6, #10, #11, and #12.



I redrew the routes to make them clearer.

![routes](assets/routes.jpg)



### 1.2 Detailed specifications

The detailed specifications may be found [here](hello-maypole/Specification.md). This document is also the answer to the 3rd question.



### 1.3 Uncertainties 

#### 1.3.1 Listing

The **Listing** word has different meanings according to DuckDuckGo by searching "define listing" with it. It means either an entry in a list or a list, which might confuse users.



#### 1.3.2 Publish

There are 2 buttons on the bottom of page #2. They are **Save as Draft** and **Publish**. The **Save as Draft** button is gray because the user hasn't inputted anything yet. The **Publish** button should perhaps be gray as well because of the same reason. If this is the case, the route from page #2 to page #11 should perhaps be changed from page #3 to page #11.



#### 1.3.3 Save as Draft

There seems no route starting from the **Save as Draft** button on page #3.



#### 1.3.4 The featured image

The featured image shown on page #5 doesn't appear on page #3. I wonder how to determine which image should be chosen as the featured image.



#### 1.3.5 Unpublish

There seems no route starting from the **Unpublish** button on page #6.



#### 1.3.6 Statuses

I can tell that there are at least 3 statuses from page #8. They are **To be dropped**, **Published**, and **Dropped**.

I can tell that there are 4 statuses from page #9. They are **Available**, **Unavailable**, **Sold Out**, and **Draft**.

I wonder how these 2 groups of status are compatible with each other.



#### 1.3.7 Required fields on the **New/Edit Item** page 

I can tell that there are at least 5 fields that are required or mandatory from page #12.

1.3.2.1 I don't understand the "To be Dropped" field and why this field is required or mandatory.

1.3.2.2 There are many more fields on page #3 than those on page #12. There might be more required fields that should be shown on page #12 in the design.



## 2. hello-maypole

### 2.1 Introduction

The **hello-maypole** app has a home page (page #0). Its routes are listed as follows:

0 ---> 1 ---> 2 <---> 8



The animation screencast in the GIF format is shown below:

![2021-3-23 22-30 hello-maypole](assets/2021-3-23 22-30 hello-maypole.gif)



### 2.2 Prerequisites

My had planed to store data with Firestore. However, the backend made in Node.js couldn't establish the connection with it successfully. So I decided to change the database to MongoDB Atlas, which provides online MongoDB service.



| MongoDB Atlas | https://account.mongodb.com/account/login |
| ------------- | ----------------------------------------- |
| Email         | boxoft@gmail.com                          |
| Password      | TestIn2021                                |

| Database User |                  |
| ------------- | ---------------- |
| Username      | dbUser           |
| Password      | epgbfZjvFQyYvb3  |
| Username      | dba              |
| Password      | l6MXMWQr44kTZnP0 |

| Database   | Maypole  |
| ---------- | -------- |
| Collection | products |

![MongoDB Atlas](assets/2021-03-23 21_58_12-MongoDB Atlas.png)



### 2.3 How the data is stored

The data is stored in a MongoDB database hosted by MongoDB Atlas.

The database schema is very simple. Its name is **Maypole**. It contains only one collection whose name is **products**. The **product** collection doesn't have any documents at first. A document contains an auto **_id** and a **title**.



### 2.4 How the data is uploaded & downloaded

Suppose the user is on the **New/Edit Item** page (page #2).

<img src="assets/2021-03-23 22_11_42-New Listing.png" alt="2021-03-23 22_11_42-New Listing" style="zoom:75%;" />



When the user clicks the **Publish** button, the `newProduct()` method defined in the `hello-maypole/src/pages/NEItem.vue` page shall be called.

```js
...
<script>
export default {
  name: "PageNEItem", // New/Edit Item
  data() {
    return {
      title: "Product ",
    };
  },
  methods: {
    newProduct() {
      this.$axios
        .post("http://localhost:3000/product", {
          title: this.title,
        })
        .then((res) => {
          // console.log("res: ", res);
        })
        .catch((err) => {
          console.error(err);
        });
    },
  },
  mounted() {
    this.$store.commit("mHeader/changeTitle", "New Listing");

    this.$store.commit("mHeader/showLeftIcon");
    this.$store.commit("mHeader/setLeftTo", "/store");

    this.$store.commit("mHeader/hideRightIcon");
  },
};
</script>
```



The backend `hello-maypole/backend/index.js` shall handle the HTTP POST request. 

```js
...
app.post('/product', (req, res) => {
    const product = new Product()
    product.title = req.body.title

    product.save((err) => {
        if (err) {
            return res.send(err)
        }

        res.json({ message: 'Product created!' })
    })
})
...
```



A **product** document shall be created in the MongoDB database.

![2021-03-23 22_21_20-MongoDB Atlas](assets/2021-03-23 22_21_20-MongoDB Atlas.png)



After that,  the **New/Edit Item** page (page #2) shall be redirected to the **My Store** page (page #8).



the `getProducts()` method defined in the `hello-maypole/src/pages/MyStore.vue` page shall be called.

```js
<script>
export default {
  name: "PageMyStore",
  data() {
    return {
      tab: "listing",
      products: [],
    };
  },
  methods: {
    getProducts() {
      this.$axios
        .get("http://localhost:3000/products")
        .then((res) => {
          // console.log("res: ", res);
          this.products = res.data;
        })
        .catch((err) => {
          console.error(err);
        });
    },
  },
  created() {
    this.getProducts();
  },
  mounted() {
    ...
  },
  updated() {
    this.getProducts();
  }
};
</script>
```



The backend `hello-maypole/backend/index.js` shall handle the HTTP GET request. 

```js
...
app.get('/products', (req, res) => {
    Product.find((err, products) => {
        if (err) {
            return res.send(err)
        }

        res.json(products)
    })
})
...
```



All the **product** documents shall be found from the MongoDB database.



The above uploading and downloading process works well. The **My Store** page (page #8) may look like this:

<img src="assets/2021-03-23 22_42_48-My Store.png" alt="2021-03-23 22_42_48-My Store" style="zoom:75%;" />



### 2.5 How the page transition is handled

The layout of the **hello-maypole** app uses the `hello-maypole/src/layouts/MyLayout` file.

The deferent part of each page is handled as `router-view`.

```js
    <q-page-container>
      <router-view />
    </q-page-container>
```



> Vue.js provides a `transition` wrapper component, allowing you to add entering/leaving transitions for any element or component in the following contexts:
>
> - Conditional rendering (using `v-if`)
> - Conditional display (using `v-show`)
> - Dynamic components
> - Component root nodes



Changing the code a little shall make the page support transition.

```js
    <q-page-container>
      <!-- <router-view /> -->
      <transition name="fade">
        <router-view />
      </transition>
    </q-page-container>
```



### 2.6 How the key NPM packages are used

#### 2.6.1 Backend

The backend locates in the `hello-maypole` folder.

Its `packages.json` file lists the necessary NPM packages.

```json
{
  "name": "backend",
  ...
  "main": "index.js",
  "scripts": {
    "start": "nodemon index.js"
  },
  ...
  "dependencies": {
    "cors": "^2.8.5",
    "express": "^4.17.1",
    "firebase-admin": "^9.5.0",
    "mongoose": "^5.12.2"
  },
  "devDependencies": {
    "nodemon": "^2.0.7"
  }
}
```



https://www.npmjs.com/package/cors

> CORS is a node.js package for providing a [Connect](http://www.senchalabs.org/connect/)/[Express](http://expressjs.com/) middleware that can be used to enable [CORS](http://en.wikipedia.org/wiki/Cross-origin_resource_sharing) with various options.



https://www.npmjs.com/package/express

Express is a very popular Web framework for Node.js.



https://www.npmjs.com/package/firebase-admin

The **Firebase Admin Node.js SDK** package should be removed since it is unnecessary.



https://www.npmjs.com/package/mongoose

> Mongoose is a [MongoDB](https://www.mongodb.org/) object modeling tool designed to work in an asynchronous environment. Mongoose supports both promises and callbacks.



https://www.npmjs.com/package/nodemon

> nodemon is a tool that helps develop node.js based applications by automatically restarting the node application when file changes in the directory are detected.



#### 2.6.2 Frontend

The frontend locates in the `hello-maypole` folder.

Its `packages.json` file lists the necessary NPM packages.

```json
{
  "name": "hello-maypole",
  ...
  "dependencies": {
    "@quasar/extras": "^1.0.0",
    "axios": "^0.21.1",
    "core-js": "^3.6.5",
    "quasar": "^1.0.0"
  },
  "devDependencies": {
    "@quasar/app": "^2.0.0"
  },
  ...
}

```



https://www.npmjs.com/package/quasar



https://www.npmjs.com/package/axios

**axios** is a very popular 

> Promise based HTTP client for the browser and node.js.



### 2.7 VueX

I had planed to use VueX to manage UI elements on the toolbar as well as the products in the database. Since time is limited, the 2nd half hasn't been implemented yet.

The source locates in the `hello-maypole/src/store` folder. The `my-store` folder may be ignored. 

The **header** has 4 states. They are defined in the `src/store/module-header/state.js` file.

The **header** has 4 getters. They are defined in the `src/store/module-header/getters.js` file.

The **header** has 6 mutations. They are defined in the `src/store/module-header/mutations.js` file.



## 3. Document for Both

The document for both frontend developers and backend developers may be found [here](hello-maypole/Specification.md).



## References

- Create an Instagram Clone with Vue JS, Quasar & Firebase - in 4 HOURS!
  https://www.youtube.com/playlist?list=PLAiDzIdBfy8h6HgfQg3namagsCUT0Y2Bs
- Quasar CLI Installation
  https://quasar.dev/quasar-cli/installation
- Create & Connect to a MongoDB Atlas Database With Node.js
  https://www.coderrocketfuel.com/article/create-and-connect-to-a-mongodb-atlas-database-with-node-js
- Enter/Leave & List Transitions — Vue.js
  https://vuejs.org/v2/guide/transitions.html
- 
- STLC (Software Testing Life Cycle) Phases, Entry, Exit Criteria
  https://www.guru99.com/software-testing-life-cycle.html
- Entry and Exit Criteria For Software Testing
  https://www.c-sharpcorner.com/UploadFile/953215/entry-and-exit-criteria-for-software-testing/
- Entry and Exit Criteria in Software Testing
  https://www.thinksys.com/qa-testing/entry-exit-criteria/