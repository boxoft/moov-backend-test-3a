# Specification for Both Frontend Developers and Backend Developers



## 1. Database Schema

| Database name   | Maypole  |
| --------------- | -------- |
| Collection name | products |

An example document

```json
{"_id":{"$oid":"6059fb6dc9b51c3a90b2ceec"},"title":"Product One","__v":{"$numberInt":"0"}}
```



## 2. Input Validation

Input validation on the backend is a must. Input validation on the frontend is ideal.



| title | required (mandatory)                              |
| ----- | ------------------------------------------------- |
|       | Its length should be no more than 100 characters. |



## 3. API

### 3.1 Get All Products

GET http://localhost:3000/products



### 3.2 Add A Product

POST http://localhost:3000/product

with

```json
{
    "title": "Product One"
}
```



## 4. Exit Criteria

If the backend gets all products from the MongoDB database successfully, it should forwards the response data to the frontend. Otherwise, it should output the error messages to the console.

If the backend adds a product to the MongoDB database successfully, it should output the response data to the console. Otherwise, it should output the error messages to the console.

If the frontend gets all products from the backend successfully, it should list them on the **My Store** page. Otherwise, it should stay on the **New/Edit Item** page and prompt the error messages to the user.

If the frontend requests to add a product and gets a successful response from the backend, it should redirect the page to the **My Store** page and prompt the success message to the user. Otherwise, it should stay on the **New/Edit Item** page and prompt the error messages to the user.

