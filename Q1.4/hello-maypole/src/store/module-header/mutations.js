export function changeTitle(state, title) {
    state.title = title
}

export function showLeftIcon(state) {
    state.leftIcon = true
}

export function hideLeftIcon(state, title) {
    state.leftIcon = false
}

export function showRightIcon(state) {
    state.rightIcon = true
}

export function hideRightIcon(state, title) {
    state.rightIcon = false
}

export function setLeftTo(state, to) {
    state.leftTo = to
}