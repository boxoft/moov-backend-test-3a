export function title(state) {
    return state.title
}

export function leftIcon(state) {
    return state.leftIcon
}

export function rightIcon(state) {
    return state.rightIcon
}

export function leftTo(state) {
    return state.leftTo
}