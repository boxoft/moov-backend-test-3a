const admin = require('firebase-admin');
const serviceAccount = require('./serviceAccountKey.json');
admin.initializeApp({
    credential: admin.credential.cert(serviceAccount)
});

const express = require('express')
const cors = require('cors')
const app = express()
const port = 3000

app.use(cors())

const db = admin.firestore();

app.get('/', (req, res) => {
    res.send('Hello World!')
})

app.get('/products', function (req, res) {
    // const snapshot = await db.collection('users').get();
    // snapshot.forEach((doc) => {
    //     console.log(doc.id, '=>', doc.data());
    // });
    let products = []
    db.collection('products').get().then(snapshot => {
        snapshot.forEach((doc) => {
            console.log(doc.id, '=>', doc.data());
            products.push(doc.data())
        })
        res.send(products)
    }).catch(error => {
        console.error(error);
    })
})

app.listen(port, () => {
    console.log(`Example app listening at http://localhost:${port}`)
})
