const express = require('express')
const cors = require('cors')
const app = express()
const mongoose = require('mongoose')
const uri = "mongodb+srv://dba:l6MXMWQr44kTZnP0@cluster0.z4t5c.mongodb.net/Maypole?retryWrites=true&w=majority"
const Product = require('./models/product')
const port = process.env.PORT || 3000

app.use(cors())
app.use(express.json())

mongoose.connect(uri, { useNewUrlParser: true, useUnifiedTopology: true })
mongoose.connection.on('error', (error) => {
    console.error(error);
})
mongoose.connection.on('open', () => {
    console.log("Connected to the MongoDB database.")
})

app.get('/', (req, res) => {
    res.send('Hello World!')
})

app.get('/products', (req, res) => {
    Product.find((err, products) => {
        if (err) {
            return res.send(err)
        }

        res.json(products)
    })
})

app.post('/product', (req, res) => {
    const product = new Product()
    product.title = req.body.title

    product.save((err) => {
        if (err) {
            return res.send(err)
        }

        res.json({ message: 'Product created!' })
    })
})

app.listen(port, () => {
    console.log(`Backend listening at http://localhost:${port}`)
})
