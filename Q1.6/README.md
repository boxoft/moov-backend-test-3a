# Q1.6

https://bitbucket.org/maypolepublic/backend-test-3a/src/master/Q1.6/



How would you organize this coding test differently if you are the hiring manager? What is the hiring manager's goal?



## My Answer

Most of the 6 questions of this coding test are very good. If I were the hiring manager, I would make the questions clearer. For example, before reading "[Trapping Rain Water](https://www.geeksforgeeks.org/trapping-rain-water/)", I was confused by question Q1.3 since I knew the fact that rainwater could infiltrate into the soil, and sometimes a heavy rain might cause mudslides.

If I were a hiring manager, I would try my best to find, attract, and recruit the right people for the company. More importantly, I would try to retain the right talents for the company.

To achieve these 2 goals, I would try to understand something first.

Question 1: What kind of people do the top executives, especially the CEO, think are the right people for the company?

Question 2: How much compensation do the top executives, especially the CEO, expect to pay for the right people?

Question 3: Which talents do the top executives, especially the CEO, hope to retain in the future?

Question 4: How much compensation do the top executives, especially the CEO, expect to pay to retain these talents in the future?

