# Q1.1

https://bitbucket.org/maypolepublic/backend-test-3a/src/master/Q1.1/



Please finish this Firebase cloud function (https://firebase.google.com/docs/functions/get-started, specifically https://firebase.google.com/docs/functions/http-events) that load products from Shopify using Shopify's graphql (https://shopify.dev/docs/admin-api/graphql/reference/products-and-collections) and return them in json with basic product info like description, price, images etc.

```typescript
// language: typescript
// environment: production
import { Request, Response } from 'express';
import { Application, Request, Router, RequestHandler, ErrorRequestHandler } from 'express';

export interface CloudFunctionRouteEntry {
  path: string;
  methods?: Array<'get' | 'post' | 'patch' | 'delete' | 'put'> | 'get' | 'post' | 'patch' | 'delete' | 'put';
  function: RequestHandler;
}

export const importShopifyProducts: CloudFunctionEntry = {
  path: '/q1/import/:shop/products',
  methods: ['post'],
  function: async (req: Request, res: Response) => {
    const { shop } = req.params;
    console.log('shop', shop);

    const { ids } = req.body as {
      ids: Array<string>; // the product ids to be loaded
    };

    ...
  },
};
```



## Development Environment

Windows 10

Node.js 14.16 LTS

Visual Studio Code 1.54



## Prerequisites

### 1. A Shopify development store with 7 products in it

| boxoft@gmail.com | SbnwWtDg94Phhab                              |
| ---------------- | -------------------------------------------- |
| URL              | my-2nd-development-store.myshopify.com       |
| Login            | my-2nd-development-store.myshopify.com/admin |



### 2. A private app associated with the Shopify development store

| App details               |                                             |
| :------------------------ | :------------------------------------------ |
| Private app name          | Shopify API                                 |
| Emergency developer email | [boxoft@gmail.com](mailto:boxoft@gmail.com) |

| Admin API     |                                                              |
| :------------ | :----------------------------------------------------------- |
| API key       | ff6444322c739c3e50d1ac9c2f2f1d91                             |
| Password      | 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                       |
| Example URL   | https://ff6444322c739c3e50d1ac9c2f2f1d91:[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@my-2nd-development-store.myshopify.com](mailto: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@my-2nd-development-store.myshopify.com)/admin/api/2021-01/orders.json |
| Shared Secret | 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                       |



### 3. Firebase CLI

To download and install the Firebase CLI run the following command:

`npm i -g firebase-tools`



### 4. A Firebase project

| Project Settings |                           |
| ---------------- | ------------------------- |
| Project name     | Try Cloud Functions       |
| Project ID       | try-cloud-functions-7fe73 |




## Solution

`Q1.1/README.md`

`Q1.1/fireshopify`

The `Q1.1/assets` folder contains several screenshots attached to the `README.md` file.

The `README.pdf` file is exported with Typora just in case you can see the screenshots in the `README.md` file.



### fireshopify

I implemented a Firebase Cloud function named `findProductsByIds` in the `functions/src/index.ts` file.

```typescript
import * as functions from "firebase-functions";
import fetch from "node-fetch";
import { Request, Response } from "express";

...

export const findProductsByIds = functions.https.onRequest(
  async (req: Request, res: Response) => {
    try {
      const endpoint =
        "https://my-2nd-development-store.myshopify.com/admin/api/2021-01/graphql.json";
      const query = `
        query findProductsByIds($ids: [ID!]!) {
          nodes(ids: $ids) {
            ... on Product {
              id
              title
              featuredImage {
                originalSrc
              }
              totalInventory
              priceRangeV2 {
                minVariantPrice {
                  amount
                  currencyCode
                }
                maxVariantPrice {
                  amount
                  currencyCode
                }
              }
            }
          }
        }`;
      const variables = {
        ids: [
          "gid://shopify/Product/6578559189183",
          "gid://shopify/Product/6578559254719",
          "gid://shopify/Product/6578559287487",
          "gid://shopify/Product/6578559353023",
          "gid://shopify/Product/6578559385791",
          "gid://shopify/Product/6578559418559",
          "gid://shopify/Product/6578559484095",
        ],
      };
      const { ids } = req.body as {
        ids: Array<string>; // the product ids to be loaded
      };
      console.log(req.body, ids);

      let body = { query, variables };
      if (ids) {
        body = { query: query, variables: { ids } };
      }
      console.log("body:", body);

      const response = await fetch(endpoint, {
        method: "POST",
        headers: {
          "Content-Type": "application/json",
          "X-Shopify-Access-Token": "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",
        },
        body: JSON.stringify(body),
      });
      const result = await response.json();
      console.log(result);
      res.send(result);
    } catch (error) {
      console.error(error);
      res.status(500).send(error);
    }
  }
);
```



After running the `npm run build` command and the `firebase serve` command, you may test the function with a browser or Postman.

`GET http://localhost:5000/try-cloud-functions-7fe73/us-central1/findProductsByIds` should return all the 7 products in the JSON format.

`GET http://localhost:5000/try-cloud-functions-7fe73/us-central1/findProductsByIds` with the body like this

```json
{
    "ids": [
        "gid://shopify/Product/6578559189183",
        "gid://shopify/Product/6578559353023",
        "gid://shopify/Product/6578559484095"
    ]
}
```

 should return the specified 3 products in the JSON format.

```json
{
    "data": {
        "nodes": [
            {
                "id": "gid://shopify/Product/6578559189183",
                "title": "dry fog",
                "featuredImage": {
                    "originalSrc": "https://cdn.shopify.com/s/files/1/0556/1591/4175/products/main-img.jpg?v=1616249600"
                },
                "totalInventory": 0,
                "priceRangeV2": {
                    "minVariantPrice": {
                        "amount": "2.0",
                        "currencyCode": "CNY"
                    },
                    "maxVariantPrice": {
                        "amount": "2.0",
                        "currencyCode": "CNY"
                    }
                }
            },
            {
                "id": "gid://shopify/Product/6578559353023",
                "title": "broken cherry",
                "featuredImage": {
                    "originalSrc": "https://cdn.shopify.com/s/files/1/0556/1591/4175/products/shutterstock168088331.jpg?v=1616249607"
                },
                "totalInventory": 0,
                "priceRangeV2": {
                    "minVariantPrice": {
                        "amount": "1.0",
                        "currencyCode": "CNY"
                    },
                    "maxVariantPrice": {
                        "amount": "1.0",
                        "currencyCode": "CNY"
                    }
                }
            },
            {
                "id": "gid://shopify/Product/6578559484095",
                "title": "aged shadow",
                "featuredImage": {
                    "originalSrc": "https://cdn.shopify.com/s/files/1/0556/1591/4175/products/Old-Shadow-Road.jpg?v=1616249612"
                },
                "totalInventory": 0,
                "priceRangeV2": {
                    "minVariantPrice": {
                        "amount": "6.0",
                        "currencyCode": "CNY"
                    },
                    "maxVariantPrice": {
                        "amount": "6.0",
                        "currencyCode": "CNY"
                    }
                }
            }
        ]
    },
    "extensions": {
        "cost": {
            "requestedQueryCost": 3,
            "actualQueryCost": 3,
            "throttleStatus": {
                "maximumAvailable": 1000,
                "currentlyAvailable": 997,
                "restoreRate": 50
            }
        }
    }
}
```




## Known Issues

### 1. The `firebase login` command fails from time to time.

![2021-03-22 10_46_10-firebase login](assets/2021-03-22 10_46_10-firebase login.png)




### 2. The `firebase deploy` command requires the Blaze plan, which can't be bought in China.

![2021-03-21 11_54_30-firebase deploy](assets/2021-03-21 11_54_30-firebase deploy.png)

![2021-03-21 11_52_51-Blaze-no-China](assets/2021-03-21 11_52_51-Blaze-no-China.png)



### 3. The source code provided in question Q1.1 might have some issues.

For example, the 'Request' identifier appears twice on line 3 and line 4.

For example, the 'CloudFunctionEntry' name cannot be found.



It is easy to modify your code with the `findProductsByIds` function defined in the `fireshopify/functions/src/index.ts` file.




## References

- Get started: write, test, and deploy your first functions
  https://firebase.google.com/docs/functions/get-started

- Getting Started with Cloud Functions for Firebase using TypeScript - Firecasts
  https://www.youtube.com/watch?v=DYfP-UIKxH0

- Call functions via HTTP requests
  https://firebase.google.com/docs/functions/http-events

- Products and Collections APIs
  https://shopify.dev/docs/admin-api/graphql/reference/products-and-collections

- Make GraphQL requests with Node and Express
  https://shopify.dev/tutorials/graphql-with-node-and-express

- How to Use GraphQL Operation Names and Variables
  https://www.shopify.com/partners/blog/graphql-operation-names-and-variables

- Firebase CLI
  https://firebaseopensource.com/projects/firebase/firebase-tools/