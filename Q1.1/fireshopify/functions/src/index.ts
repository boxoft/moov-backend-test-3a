import * as functions from "firebase-functions";
import fetch from "node-fetch";
import { Request, Response } from "express";
// import { Application, Request, Router, RequestHandler, ErrorRequestHandler } from 'express';

// export interface CloudFunctionRouteEntry {
//   path: string;
//   methods?: Array<'get' | 'post' | 'patch' | 'delete' | 'put'> | 'get' | 'post' | 'patch' | 'delete' | 'put';
//   function: RequestHandler;
// }

// export const importShopifyProducts: CloudFunctionEntry = {
//   path: '/q1/import/:shop/products',
//   methods: ['post'],
//   function: async (req: Request, res: Response) => {
//     const { shop } = req.params;
//     console.log('shop', shop);

//     const { ids } = req.body as {
//       ids: Array<string>; // the product ids to be loaded
//     };

//     // ...
//   },
// };

// Start writing Firebase Functions
// https://firebase.google.com/docs/functions/typescript

export const helloWorld = functions.https.onRequest((request, response) => {
  functions.logger.info("Hello logs!", { structuredData: true });
  response.send("Hello from Firebase!");
});

// GET http://localhost:5000/try-cloud-functions-7fe73/us-central1/findProducts
// should return all the 7 products in the JSON format.
//
// GET http://localhost:5000/try-cloud-functions-7fe73/us-central1/findProducts?n=3
// should return the first 3 products in the JSON format.

export const findProducts = functions.https.onRequest((request, response) => {
  const n = Number(request.query.n) || 10;

  fetch(
    "https://my-2nd-development-store.myshopify.com/admin/api/2021-01/graphql.json",
    {
      method: "POST",
      headers: {
        "Content-Type": "application/json",
        "X-Shopify-Access-Token": "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",
      },
      body: JSON.stringify({
        query: `query findProducts($num: Int!) {
                products(first: $num) {
                  edges {
                    cursor
                    node {
                      id
                      title
                      featuredImage {
                        originalSrc
                      }
                      totalInventory
                      priceRangeV2 {
                        minVariantPrice {
                          amount
                          currencyCode
                        }
                        maxVariantPrice {
                          amount
                          currencyCode
                        }
                      }
                    }
                  }
                }
              }`,
        variables: { num: n },
      }),
    }
  )
    .then((result) => {
      return result.json();
    })
    .then((data) => {
      console.log("data returned:\n", data);
      response.send(data);
    });
});

// GET http://localhost:5000/try-cloud-functions-7fe73/us-central1/findProductsByIds
// should return all the 7 products in the JSON format.
//
// GET http://localhost:5000/try-cloud-functions-7fe73/us-central1/findProductsByIds
// with the body like this:
// {
//   "ids": [
//       "gid://shopify/Product/6578559189183",
//       "gid://shopify/Product/6578559353023",
//       "gid://shopify/Product/6578559484095"
//   ]
// }
// should return the 3 specified 3 products in the JSON format.

export const findProductsByIds = functions.https.onRequest(
  async (req: Request, res: Response) => {
    try {
      const endpoint =
        "https://my-2nd-development-store.myshopify.com/admin/api/2021-01/graphql.json";
      const query = `
        query findProductsByIds($ids: [ID!]!) {
          nodes(ids: $ids) {
            ... on Product {
              id
              title
              featuredImage {
                originalSrc
              }
              totalInventory
              priceRangeV2 {
                minVariantPrice {
                  amount
                  currencyCode
                }
                maxVariantPrice {
                  amount
                  currencyCode
                }
              }
            }
          }
        }`;
      const variables = {
        ids: [
          "gid://shopify/Product/6578559189183",
          "gid://shopify/Product/6578559254719",
          "gid://shopify/Product/6578559287487",
          "gid://shopify/Product/6578559353023",
          "gid://shopify/Product/6578559385791",
          "gid://shopify/Product/6578559418559",
          "gid://shopify/Product/6578559484095",
        ],
      };
      const { ids } = req.body as {
        ids: Array<string>; // the product ids to be loaded
      };
      console.log(req.body, ids);

      let body = { query, variables };
      if (ids) {
        body = { query: query, variables: { ids } };
      }
      console.log("body:", body);

      const response = await fetch(endpoint, {
        method: "POST",
        headers: {
          "Content-Type": "application/json",
          "X-Shopify-Access-Token": "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",
        },
        body: JSON.stringify(body),
      });
      const result = await response.json();
      console.log(result);
      res.send(result);
    } catch (error) {
      console.error(error);
      res.status(500).send(error);
    }
  }
);
