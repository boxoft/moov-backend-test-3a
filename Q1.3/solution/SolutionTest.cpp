#include <assert.h>
#include "Solution.h"

void test1()
{
    int height_example[] = {1, 4, 3, 1, 3, 2,
                            3, 2, 1, 3, 2, 4,
                            2, 3, 3, 2, 3, 1};

    assert(water_volume(height_example, 3, 6) == 4);
}

void test2()
{
    int height_example[] = {5, 5, 5,
                            5, 1, 5,
                            5, 5, 5};

    assert(water_volume(height_example, 3, 3) == 4);
}

void test3()
{
    int height_example[] = {3, 2, 2, 2, 4,
                            3, 0, 2, 0, 4,
                            3, 3, 3, 2, 4};

    assert(water_volume(height_example, 3, 5) == 4);
}

void test4()
{
    int height_example[] = {3, 3, 3, 3, 4,
                            3, 0, 2, 0, 4,
                            3, 3, 3, 3, 4};

    assert(water_volume(height_example, 3, 5) == 7);
}

int main()
{
    test1();
    test2();
    test3();
    test4();

    return 0;
}
