#include <iostream>
using namespace std;
#include <queue>
#include "Solution.h"

int water_volume(const int *height, int m, int n)
{
    priority_queue<pair<int, int>, vector<pair<int, int>>, greater<pair<int, int>>> pq;
    bool visited[m * n] = {false};

    // The top row and the bottom row can't hold water.
    for (int i = 0; i < n; i++)
    {
        int pos = i;
        visited[pos] = true;
        pq.push({height[pos], pos});

        pos = (m - 1) * n + i;
        visited[pos] = true;
        pq.push({height[pos], pos});
    }

    // The leftmost column and the rightmost column can't hold water either.
    for (int i = 1; i < m - 1; ++i)
    {
        int pos = i * n;
        visited[pos] = true;
        pq.push({height[pos], pos});

        pos = i * n + n - 1;
        visited[pos] = true;
        pq.push({height[pos], pos});
    }

    int volume = 0;            // Water volume
    int level = 0;             // Water level
    int dx[4] = {-1, 1, 0, 0}; // left / right
    int dy[4] = {0, 0, -1, 1}; // top / bottom

    // Traverse all the cells.
    cout << "\t\tHeight\tPosition" << endl;
    while (!pq.empty())
    {
        // Get the cell with the lowest height from the Min Heap.
        auto current = pq.top();
        pq.pop();

        int currentHeight = current.first;
        int currentPosition = current.second;
        cout << "  Current Cell: " << currentHeight << "\t" << currentPosition << endl;

        // Raise the water level if necessary.
        level = max(level, currentHeight);

        // Check the 4 adjacent cells of the current cell.
        for (int i = 0; i < 4; i++)
        {
            int x = currentPosition / n + dx[i];
            int y = currentPosition % n + dy[i];
            int pos = x * n + y; // The position of the adjacent cell.

            if (x < 0 || x >= m || y < 0 || y >= n || visited[pos])
                continue;

            visited[pos] = true;
            cout << " Adjacent Cell: " << height[pos] << "\t" << pos << endl;
            if (height[pos] < level)
                volume += level - height[pos];
            pq.push({height[pos], pos});
        }
    }

    return volume;
}