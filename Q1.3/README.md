# Q1.3

https://bitbucket.org/maypolepublic/backend-test-3a/src/master/Q1.3/



Given a height map ( height is an array ) of a land ( m x n ), please calculate how much water it can hold after a heavy rain.

![](https://storage.googleapis.com/public-assets-maypole/interview-assets/image%200.png)

```C++
int water_volume(const int height*, int m, int n){
    // please fill in this part
}

// For example:

int *height_example = {5, 5, 5, 
        5, 1, 5, 
        5, 5, 5};

assert (water_volume(height_example, 3, 3) == 4 ); 
```



## Development Environment

Ubuntu 20.04 LTS in VirtualBox

g++ 9.3.0

Visual Studio Code 1.54



## Solution

`Q1.3/README.md`

`Q1.3/solution`



There are four test cases defined in the `solution/SolutionTest.cpp` file.

```c++
#include <assert.h>
#include "Solution.h"

void test1()
{
    int height_example[] = {1, 4, 3, 1, 3, 2,
                            3, 2, 1, 3, 2, 4,
                            2, 3, 3, 2, 3, 1};

    assert(water_volume(height_example, 3, 6) == 4);
}

void test2()
{
    int height_example[] = {5, 5, 5,
                            5, 1, 5,
                            5, 5, 5};

    assert(water_volume(height_example, 3, 3) == 4);
}

void test3()
{
    int height_example[] = {3, 2, 2, 2, 4,
                            3, 0, 2, 0, 4,
                            3, 3, 3, 2, 4};

    assert(water_volume(height_example, 3, 5) == 4);
}

void test4()
{
    int height_example[] = {3, 3, 3, 3, 4,
                            3, 0, 2, 0, 4,
                            3, 3, 3, 3, 4};

    assert(water_volume(height_example, 3, 5) == 7);
}

int main()
{
    test1();
    test2();
    test3();
    test4();

    return 0;
}
```



Running the `make && make test` command shall see that all the four test cases have been passed and the whole flow of the solution.

```bash
g++ -g -std=c++17 -Wall Solution.cpp SolutionTest.cpp -o SolutionTest
./SolutionTest
                Height  Position
  Current Cell: 1       0
  Current Cell: 1       3
 Adjacent Cell: 3       9
  Current Cell: 1       17
  Current Cell: 2       5
  Current Cell: 2       12
  Current Cell: 2       15
  Current Cell: 3       2
 Adjacent Cell: 1       8
  Current Cell: 1       8
 Adjacent Cell: 2       7
  Current Cell: 2       7
  Current Cell: 3       4
 Adjacent Cell: 2       10
  Current Cell: 2       10
  Current Cell: 3       6
  Current Cell: 3       9
  Current Cell: 3       13
  Current Cell: 3       14
  Current Cell: 3       16
  Current Cell: 4       1
  Current Cell: 4       11
                Height  Position
  Current Cell: 5       0
  Current Cell: 5       1
 Adjacent Cell: 1       4
  Current Cell: 1       4
  Current Cell: 5       2
  Current Cell: 5       3
  Current Cell: 5       5
  Current Cell: 5       6
  Current Cell: 5       7
  Current Cell: 5       8
                Height  Position
  Current Cell: 2       1
 Adjacent Cell: 0       6
  Current Cell: 0       6
 Adjacent Cell: 2       7
  Current Cell: 2       2
  Current Cell: 2       3
 Adjacent Cell: 0       8
  Current Cell: 0       8
  Current Cell: 2       7
  Current Cell: 2       13
  Current Cell: 3       0
  Current Cell: 3       5
  Current Cell: 3       10
  Current Cell: 3       11
  Current Cell: 3       12
  Current Cell: 4       4
  Current Cell: 4       9
  Current Cell: 4       14
                Height  Position
  Current Cell: 3       0
  Current Cell: 3       1
 Adjacent Cell: 0       6
  Current Cell: 0       6
 Adjacent Cell: 2       7
  Current Cell: 2       7
 Adjacent Cell: 0       8
  Current Cell: 0       8
  Current Cell: 3       2
  Current Cell: 3       3
  Current Cell: 3       5
  Current Cell: 3       10
  Current Cell: 3       11
  Current Cell: 3       12
  Current Cell: 3       13
  Current Cell: 4       4
  Current Cell: 4       9
  Current Cell: 4       14
```



The `water_volume()` function is defined in the `solution/Solution.cpp` file.

```c++
#include <iostream>
using namespace std;
#include <queue>
#include "Solution.h"

int water_volume(const int *height, int m, int n)
{
    priority_queue<pair<int, int>, vector<pair<int, int>>, greater<pair<int, int>>> pq;
    bool visited[m * n] = {false};

    // The top row and the bottom row can't hold water.
    for (int i = 0; i < n; i++)
    {
        int pos = i;
        visited[pos] = true;
        pq.push({height[pos], pos});

        pos = (m - 1) * n + i;
        visited[pos] = true;
        pq.push({height[pos], pos});
    }

    // The leftmost column and the rightmost column can't hold water either.
    for (int i = 1; i < m - 1; ++i)
    {
        int pos = i * n;
        visited[pos] = true;
        pq.push({height[pos], pos});

        pos = i * n + n - 1;
        visited[pos] = true;
        pq.push({height[pos], pos});
    }

    int volume = 0;            // Water volume
    int level = 0;             // Water level
    int dx[4] = {-1, 1, 0, 0}; // left / right
    int dy[4] = {0, 0, -1, 1}; // top / bottom

    // Traverse all the cells.
    cout << "\t\tHeight\tPosition" << endl;
    while (!pq.empty())
    {
        // Get the cell with the lowest height from the Min Heap.
        auto current = pq.top();
        pq.pop();

        int currentHeight = current.first;
        int currentPosition = current.second;
        cout << "  Current Cell: " << currentHeight << "\t" << currentPosition << endl;

        // Raise the water level if necessary.
        level = max(level, currentHeight);

        // Check the 4 adjacent cells of the current cell.
        for (int i = 0; i < 4; i++)
        {
            int x = currentPosition / n + dx[i];
            int y = currentPosition % n + dy[i];
            int pos = x * n + y; // The position of the adjacent cell.

            if (x < 0 || x >= m || y < 0 || y >= n || visited[pos])
                continue;

            visited[pos] = true;
            cout << " Adjacent Cell: " << height[pos] << "\t" << pos << endl;
            if (height[pos] < level)
                volume += level - height[pos];
            pq.push({height[pos], pos});
        }
    }

    return volume;
}
```



## References

- Trapping Rain Water
  https://www.geeksforgeeks.org/trapping-rain-water/

- Trapping Rain Water II
  https://shirleyisnotageek.blogspot.com/2016/11/trapping-rain-water-ii.html

- 407. Trapping Rain Water II (Hard)
  https://xiaoguan.gitbooks.io/leetcode/content/LeetCode/407-trapping-rain-water-ii.html


